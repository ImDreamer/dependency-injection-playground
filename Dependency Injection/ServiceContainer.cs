﻿using System;
using Dependency_Injection.Descriptors;
using JetBrains.Annotations;

namespace Dependency_Injection {
	public class ServiceContainer {
		private readonly ServiceCollection _serviceCollection = new();

		[UsedImplicitly]
		public void Register<TImplementation>(ServiceLifeTime serviceLifeTime)
			where TImplementation : class {
			CheckImplementationForValidity(typeof(TImplementation));
			_serviceCollection.AddService(typeof(TImplementation),
				ServiceDescriptorFactory.BuildDescriptor<TImplementation>(serviceLifeTime));
		}

		[UsedImplicitly]
		public void Register<TService>(TService implementation, ServiceLifeTime serviceLifeTime)
			where TService : class {
			CheckImplementationForValidity(implementation);
			_serviceCollection.AddService(typeof(TService),
				ServiceDescriptorFactory.BuildDescriptor(implementation, serviceLifeTime));
		}

		[UsedImplicitly]
		public void Register<TService, TImplementation>(ServiceLifeTime serviceLifeTime)
			where TService : class
			where TImplementation : class, TService {
			CheckServiceInterfaceForValidity(typeof(TService));
			CheckImplementationForValidity(typeof(TImplementation));
			_serviceCollection.AddService(typeof(TService),
				ServiceDescriptorFactory.BuildDescriptor<TImplementation>(serviceLifeTime));
		}

		[UsedImplicitly]
		public void RegisterSingleton<TImplementation>()
			where TImplementation : class {
			Register<TImplementation>(ServiceLifeTime.Singleton);
		}

		[UsedImplicitly]
		public void RegisterSingleton<TService>(TService implementation)
			where TService : class {
			Register(implementation, ServiceLifeTime.Singleton);
		}

		[UsedImplicitly]
		public void RegisterSingleton<TService, TImplementation>()
			where TService : class
			where TImplementation : class, TService {
			Register<TService, TImplementation>(ServiceLifeTime.Singleton);
		}

		[UsedImplicitly]
		public void RegisterTransient<TImplementation>()
			where TImplementation : class {
			Register<TImplementation>(ServiceLifeTime.Transient);
		}

		[UsedImplicitly]
		public void RegisterTransient<TService>(TService implementation)
			where TService : class {
			Register(implementation, ServiceLifeTime.Transient);
		}

		[UsedImplicitly]
		public void RegisterTransient<TService, TImplementation>()
			where TService : class
			where TImplementation : class, TService {
			Register<TService, TImplementation>(ServiceLifeTime.Transient);
		}

		[UsedImplicitly]
		[Pure]
		public TService GetService<TService>()
			where TService : class {
			return _serviceCollection.GetService<TService>();
		}

		private static void CheckImplementationForValidity<TImplementation>(TImplementation implementation)
			where TImplementation : class {
			if (implementation is Type type) {
				if (type.IsInterface)
					throw new NotSupportedException("The implementation can't be an interface.");
				if (type.IsAbstract)
					throw new NotSupportedException("The implementation can't be an abstract class");
				return;
			}

			if (implementation.GetType().IsInterface)
				throw new NotSupportedException("The implementation can't be an interface.");
			if (implementation.GetType().IsAbstract)
				throw new NotSupportedException("The implementation can't be an abstract class");
		}

		private static void CheckServiceInterfaceForValidity<TService>(TService implementation)
			where TService : class {
			if (implementation is Type type) {
				if (!type.IsInterface)
					throw new NotSupportedException("The interface should be an interface.");
				return;
			}

			if (!implementation.GetType().IsInterface)
				throw new NotSupportedException("The interface should be an interface.");
		}
	}
}