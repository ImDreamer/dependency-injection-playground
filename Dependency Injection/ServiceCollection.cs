﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Dependency_Injection.Descriptors;
using JetBrains.Annotations;

namespace Dependency_Injection {
	public class ServiceCollection {
		private readonly Dictionary<Type, IServiceDescriptor> _services = new();

		[UsedImplicitly]
		public void AddService(Type service, IServiceDescriptor descriptor) {
			if (_services.ContainsKey(service))
				throw new DuplicateNameException("A service with that type has already been added.");
			_services.Add(service, descriptor);
		}

		[UsedImplicitly]
		[Pure]
		public TService GetService<TService>() {
			return (TService) GetService(typeof(TService));
		}

		[UsedImplicitly]
		[Pure]
		public object GetService(Type type) {
			IServiceDescriptor descriptor = GetServiceDescriptor(type);
			if (descriptor is not SingletonServiceDescriptor singleton) return CreateInstance(descriptor.ServiceType);
			if (singleton.Initialized) return descriptor.Instance;
			object instance = CreateInstance(descriptor.ServiceType);
			singleton.SetInstance(instance);

			return descriptor.Instance;
		}

		[UsedImplicitly]
		[Pure]
		public IServiceDescriptor GetServiceDescriptor<TService>() {
			return GetServiceDescriptor(typeof(TService));
		}

		[UsedImplicitly]
		[Pure]
		public IServiceDescriptor GetServiceDescriptor(Type type) {
			if (!_services.ContainsKey(type))
				throw new ArgumentException($"No service found with the name {type.Name}");
			return _services[type];
		}

		[Pure]
		private object CreateInstance(Type type) {
			if (type.GetConstructors().Length == 0 ||
			    // ReSharper disable once HeapView.ClosureAllocation
			    type.GetConstructors().Any(info => info.GetParameters().Length == 0))
				return Activator.CreateInstance(type);


			ConstructorInfo constructor = type.GetConstructors().First();

			if (IsSelfDependent(type, constructor))
				throw new NotSupportedException("You can't depend on yourself dummy");


			if (constructor.GetParameters().Any(param => IsCircularlyDependent(type, param)))
				throw new NotSupportedException("You can't depend on something that depends on you dummy");


			// ReSharper disable once HeapView.ObjectAllocation
			// ReSharper disable once HeapView.ClosureAllocation
			// ReSharper disable once HeapView.DelegateAllocation
			object[] parameters = constructor.GetParameters().Select(info => GetService(info.ParameterType))
				.ToArray();

			return Activator.CreateInstance(type, parameters);
		}

		private static bool IsSelfDependent(Type type, ConstructorInfo constructor) {
			return constructor.GetParameters().Any(info => info.GetType() == type) ||
			       constructor.GetParameters().Any(info => type.GetInterface(info.ParameterType.Name, true) != null);
		}

		private bool IsCircularlyDependent(Type type, ParameterInfo parameterInfo) {
			IServiceDescriptor type2 = _services[parameterInfo.ParameterType];

			if (type2.ServiceType.GetConstructors().Length == 0 ||
			    // ReSharper disable once HeapView.ClosureAllocation
			    type2.ServiceType.GetConstructors().Any(info => info.GetParameters().Length == 0))
				return false;

			return type2.ServiceType.GetConstructors().First().GetParameters()
				.Any(info => type == _services[info.ParameterType].ServiceType);
		}
	}
}