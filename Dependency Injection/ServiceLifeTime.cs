﻿namespace Dependency_Injection {
	public enum ServiceLifeTime {
		Singleton,
		Transient
	}
}