﻿using System;

namespace Dependency_Injection.Descriptors {
	public class SingletonServiceDescriptor : IServiceDescriptor {
		public bool Initialized { get; private set; }

		public SingletonServiceDescriptor(Type serviceType) {
			ServiceType = serviceType;
			ServiceLifeTime = ServiceLifeTime.Singleton;
		}

		public object Instance { get; private set; }
		public ServiceLifeTime ServiceLifeTime { get; }

		public Type ServiceType { get; }

		public void SetInstance(object instance) {
			if (Initialized) throw new NotSupportedException("You can't update a instance on initialized service.");
			Instance = instance;
			Initialized = true;
		}
	}
}