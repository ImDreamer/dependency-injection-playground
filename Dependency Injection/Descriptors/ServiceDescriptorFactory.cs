﻿using System;
using System.Diagnostics.Contracts;

namespace Dependency_Injection.Descriptors {
	public static class ServiceDescriptorFactory {
		[Pure]
		public static IServiceDescriptor BuildDescriptor<TImplementation>(TImplementation implementation,
			ServiceLifeTime serviceLifeTime) where TImplementation : class {
			return BuildDescriptor(implementation.GetType(), serviceLifeTime);
		}

		[Pure]
		public static IServiceDescriptor BuildDescriptor<TImplementation>(ServiceLifeTime serviceLifeTime)
			where TImplementation : class {
			return BuildDescriptor(typeof(TImplementation), serviceLifeTime);
		}

		[Pure]
		private static IServiceDescriptor BuildDescriptor(Type type, ServiceLifeTime serviceLifeTime) {
			IServiceDescriptor descriptor = serviceLifeTime switch {
				// ReSharper disable once HeapView.ObjectAllocation.Evident
				ServiceLifeTime.Singleton => new SingletonServiceDescriptor(type),
				// ReSharper disable once HeapView.ObjectAllocation.Evident
				ServiceLifeTime.Transient => new TransientServiceDescriptor(type),
				_ => throw new ArgumentOutOfRangeException(nameof(serviceLifeTime), serviceLifeTime, null)
			};
			return descriptor;
		}
	}
}