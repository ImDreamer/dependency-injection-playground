﻿using System;

namespace Dependency_Injection.Descriptors {
	public class TransientServiceDescriptor : IServiceDescriptor {
		public TransientServiceDescriptor(Type serviceType) {
			ServiceType = serviceType;
			ServiceLifeTime = ServiceLifeTime.Singleton;
		}

		public object Instance => default;
		public ServiceLifeTime ServiceLifeTime { get; }

		public Type ServiceType { get; }
	}
}