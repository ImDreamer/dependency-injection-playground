﻿using System;

namespace Dependency_Injection.Descriptors {
	public interface IServiceDescriptor {
		public object Instance { get; }
		public ServiceLifeTime ServiceLifeTime { get; }
		public Type ServiceType { get; }
	}
}