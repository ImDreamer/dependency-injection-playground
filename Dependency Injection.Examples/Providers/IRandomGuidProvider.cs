﻿using System;

namespace Dependency_Injection.Examples.Providers {
	public interface IRandomGuidProvider {
		Guid GenerateGuid { get; }
	}
}