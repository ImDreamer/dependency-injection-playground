﻿using System;
using JetBrains.Annotations;

namespace Dependency_Injection.Examples.Providers {
	[UsedImplicitly]
	internal class SomeOtherService : ISomeOtherService {
		private readonly IRandomNumberProvider _randomNumberProvider;
		private readonly ISomeService _someService;

		public SomeOtherService(ISomeService someService, IRandomNumberProvider randomNumberProvider) {
			_someService = someService;
			_randomNumberProvider = randomNumberProvider;
		}


		public void DeferredPrint() {
			_someService.Print();
			Console.WriteLine($"Random Number: {_randomNumberProvider.RandomInt.ToString()}");
		}
	}
}