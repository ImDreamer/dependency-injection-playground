﻿using System;
using JetBrains.Annotations;

namespace Dependency_Injection.Examples.Providers {
	[UsedImplicitly]
	internal class SomeService : ISomeService {
		private readonly IRandomGuidProvider _randomGuidProvider;

		public SomeService(IRandomGuidProvider randomGuidProvider) {
			_randomGuidProvider = randomGuidProvider;
		}

		public void Print() {
			Console.WriteLine(_randomGuidProvider.GenerateGuid.ToString());
		}
	}
}