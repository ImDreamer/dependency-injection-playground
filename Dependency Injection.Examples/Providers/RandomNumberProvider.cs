﻿using System;
using JetBrains.Annotations;

namespace Dependency_Injection.Examples.Providers {
	[UsedImplicitly]
	internal class RandomNumberProvider : IRandomNumberProvider {
		private static readonly Random Random = new();

		private int _randomInt;

		public int RandomInt {
			get {
				if (_randomInt == 0)
					_randomInt = Random.Next(0, 1000);
				return _randomInt;
			}
		}
	}
}