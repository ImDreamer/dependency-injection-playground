﻿namespace Dependency_Injection.Examples.Providers {
	public interface IRandomNumberProvider {
		int RandomInt { get; }
	}
}