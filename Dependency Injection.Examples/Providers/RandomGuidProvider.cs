﻿using System;
using JetBrains.Annotations;

namespace Dependency_Injection.Examples.Providers {
	[UsedImplicitly]
	public class RandomGuidProvider : IRandomGuidProvider {
		private Guid _guid;

		public Guid GenerateGuid {
			get {
				if (_guid == default)
					_guid = Guid.NewGuid();
				return _guid;
			}
		}
	}
}