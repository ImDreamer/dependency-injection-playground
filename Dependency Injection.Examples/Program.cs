﻿using System;
using Dependency_Injection.Examples.Providers;

namespace Dependency_Injection.Examples {
	internal static class Program {
		private static readonly ServiceContainer ServiceContainer = new();

		private static void Main() {
			ServiceContainer.RegisterSingleton<IRandomGuidProvider, RandomGuidProvider>();
			ServiceContainer.RegisterTransient<IRandomNumberProvider, RandomNumberProvider>();
			ServiceContainer.RegisterSingleton<ISomeService, SomeService>();
			ServiceContainer.RegisterTransient<ISomeOtherService, SomeOtherService>();

			Console.WriteLine("Singleton - Should be the same every time.");
			for (var i = 0; i < 10; i++) ServiceContainer.GetService<ISomeService>().Print();

			Console.WriteLine("\nTransient - Random Number should be different every time.");
			for (var i = 0; i < 10; i++) ServiceContainer.GetService<ISomeOtherService>().DeferredPrint();
		}
	}
}