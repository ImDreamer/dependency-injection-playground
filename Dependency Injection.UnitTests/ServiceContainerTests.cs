using System;
using Dependency_Injection.UnitTests.Providers;
using FluentAssertions;
using Xunit;

namespace Dependency_Injection.UnitTests {
	public class ServiceContainerTests {
		[Fact]
		public void RegisterSingleton_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterSingleton<RandomGuidProvider>();
			var guidProvider = serviceContainer.GetService<RandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterSingleton_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			Action act = () => serviceContainer.RegisterSingleton<IRandomGuidProvider>();

			//Assert
			act.Should().ThrowExactly<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void RegisterSingleton_With_Implementation_And_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterSingleton<IRandomGuidProvider>(new RandomGuidProvider());
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void RegisterSingleton_With_Implementation_Only() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterSingleton(new RandomGuidProvider());
			var guidProvider = serviceContainer.GetService<RandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterSingleton_With_Interface_Only() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterSingleton(typeof(IRandomGuidProvider));

			//Assert
			act.Should().ThrowExactly<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void Fail_RegisterSingleton_With_Abstract() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterSingleton(typeof(AbstractRandomGuidProvider));

			//Assert
			act.Should().ThrowExactly<NotSupportedException>()
				.WithMessage("The implementation can't be an abstract class");
		}

		[Fact]
		public void RegisterSingleton_Both_With_Generic_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			serviceContainer.RegisterSingleton<IRandomGuidProvider, RandomGuidProvider>();
			//Act
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterSingleton_Both_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterSingleton<RandomGuidProvider, RandomGuidProvider>();

			//Assert
			act.Should().Throw<NotSupportedException>().WithMessage("The interface should be an interface.");
		}

		[Fact]
		public void Fail_RegisterSingleton_Both_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterSingleton<IRandomGuidProvider, IRandomGuidProvider>();

			//Assert
			act.Should().Throw<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void GetService_Singleton() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			serviceContainer.RegisterSingleton<IRandomGuidProvider, RandomGuidProvider>();
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();
			var guidProvider2 = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
			guidProvider2.Should().NotBeNull();
			guidProvider2.GenerateGuid.Should().NotBeEmpty();

			guidProvider.Should().BeSameAs(guidProvider2);
		}


		[Fact]
		public void RegisterTransient_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterTransient<RandomGuidProvider>();
			var guidProvider = serviceContainer.GetService<RandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterTransient_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			Action act = () => serviceContainer.RegisterTransient<IRandomGuidProvider>();

			//Assert
			act.Should().ThrowExactly<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void RegisterTransient_With_Implementation_And_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterTransient<IRandomGuidProvider>(new RandomGuidProvider());
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void RegisterTransient_With_Implementation_Only() {
			//Arrange
			var serviceContainer = new ServiceContainer();

			//Act
			serviceContainer.RegisterTransient(new RandomGuidProvider());
			var guidProvider = serviceContainer.GetService<RandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterTransient_With_Interface_Only() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterTransient(typeof(IRandomGuidProvider));

			//Assert
			act.Should().ThrowExactly<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void Fail_RegisterTransient_With_Abstract() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterTransient(typeof(AbstractRandomGuidProvider));

			//Assert
			act.Should().ThrowExactly<NotSupportedException>()
				.WithMessage("The implementation can't be an abstract class");
		}

		[Fact]
		public void RegisterTransient_Both_With_Generic_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			serviceContainer.RegisterTransient<IRandomGuidProvider, RandomGuidProvider>();
			//Act
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
		}

		[Fact]
		public void Fail_RegisterTransient_Both_With_Generic() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterTransient<RandomGuidProvider, RandomGuidProvider>();

			//Assert
			act.Should().Throw<NotSupportedException>().WithMessage("The interface should be an interface.");
		}

		[Fact]
		public void Fail_RegisterTransient_Both_Interface() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			Action act = () => serviceContainer.RegisterTransient<IRandomGuidProvider, IRandomGuidProvider>();

			//Assert
			act.Should().Throw<NotSupportedException>().WithMessage("The implementation can't be an interface.");
		}

		[Fact]
		public void GetService_Transient() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			//Act
			serviceContainer.RegisterTransient<IRandomGuidProvider, RandomGuidProvider>();
			var guidProvider = serviceContainer.GetService<IRandomGuidProvider>();
			var guidProvider2 = serviceContainer.GetService<IRandomGuidProvider>();

			//Assert
			guidProvider.Should().NotBeNull();
			guidProvider.GenerateGuid.Should().NotBeEmpty();
			guidProvider2.Should().NotBeNull();
			guidProvider2.GenerateGuid.Should().NotBeEmpty();

			guidProvider.Should().NotBeSameAs(guidProvider2);
		}
	}
}