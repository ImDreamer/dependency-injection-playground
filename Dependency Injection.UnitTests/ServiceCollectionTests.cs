﻿using System;
using System.Data;
using Dependency_Injection.Descriptors;
using Dependency_Injection.UnitTests.Providers;
using FluentAssertions;
using Xunit;

namespace Dependency_Injection.UnitTests {
	public class ServiceCollectionTests {
		[Fact]
		public void AddService() {
			//Arrange
			var serviceCollection = new ServiceCollection();
			IServiceDescriptor mockDescriptor =
				ServiceDescriptorFactory.BuildDescriptor<IRandomGuidProvider>(ServiceLifeTime.Singleton);

			//Act
			serviceCollection.AddService(typeof(IRandomGuidProvider), mockDescriptor);

			//Assert
			IServiceDescriptor descriptor = serviceCollection.GetServiceDescriptor<IRandomGuidProvider>();
			descriptor.Should().Be(mockDescriptor);
			descriptor.Instance.Should().BeNull();
			descriptor.ServiceLifeTime.Should().Be(ServiceLifeTime.Singleton);
		}

		[Fact]
		public void Fail_AddService_Duplicate() {
			//Arrange
			var serviceCollection = new ServiceCollection();
			IServiceDescriptor mockDescriptor =
				ServiceDescriptorFactory.BuildDescriptor<IRandomGuidProvider>(ServiceLifeTime.Singleton);
			serviceCollection.AddService(typeof(IRandomGuidProvider), mockDescriptor);

			//Act
			Action act = () => serviceCollection.AddService(typeof(IRandomGuidProvider), mockDescriptor);

			//Assert
			act.Should().ThrowExactly<DuplicateNameException>()
				.WithMessage("A service with that type has already been added.");
		}

		[Fact]
		public void GetService() {
			//Arrange
			var serviceCollection = new ServiceCollection();
			IServiceDescriptor mockDescriptor =
				ServiceDescriptorFactory.BuildDescriptor(new RandomGuidProvider(),
					ServiceLifeTime.Singleton);
			serviceCollection.AddService(typeof(IRandomGuidProvider), mockDescriptor);

			//Act
			var service = serviceCollection.GetService<IRandomGuidProvider>();

			//Assert
			service.Should().NotBeNull();
		}

		[Fact]
		public void GetServiceDescriptor() {
			//Arrange
			var serviceCollection = new ServiceCollection();
			IServiceDescriptor mockDescriptor =
				ServiceDescriptorFactory.BuildDescriptor<IRandomGuidProvider>(ServiceLifeTime.Singleton);
			serviceCollection.AddService(typeof(IRandomGuidProvider), mockDescriptor);

			//Act
			IServiceDescriptor descriptor = serviceCollection.GetServiceDescriptor<IRandomGuidProvider>();

			//Assert
			descriptor.Should().Be(mockDescriptor);
			descriptor.Instance.Should().BeNull();
			descriptor.ServiceLifeTime.Should().Be(ServiceLifeTime.Singleton);
		}

		[Fact]
		public void GetService_Dependency_Resolve() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			serviceContainer.RegisterSingleton<ISomeService, SomeService>();
			serviceContainer.RegisterTransient<IRandomGuidProvider, RandomGuidProvider>();

			//Act
			var service = serviceContainer.GetService<ISomeService>();
			var service2 = serviceContainer.GetService<ISomeService>();

			//Assert
			service.Should().NotBeNull();
			service.GetGuid().Should().Be(service2.GetGuid());
		}

		[Fact]
		public void GetServiceDescriptor_NoElement() {
			//Arrange
			var serviceCollection = new ServiceCollection();

			//Act
			Action act = () => {
				IServiceDescriptor _ = serviceCollection.GetServiceDescriptor<IRandomGuidProvider>();
			};

			//Assert
			act.Should().ThrowExactly<ArgumentException>().WithMessage("No service found with the name *");
		}

		[Fact]
		public void IsSelfDependent() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			serviceContainer.Register<ISelfDepend, SelfDepend>(ServiceLifeTime.Singleton);

			//Act
			Action act = () => {
				var service = serviceContainer.GetService<ISelfDepend>();
			};

			//Assert
			act.Should().Throw<NotSupportedException>().WithMessage("You can't depend on yourself dummy");
		}

		[Fact]
		public void IsCircularDependent() {
			//Arrange
			var serviceContainer = new ServiceContainer();
			serviceContainer.Register<ICircularDepend, CircularDepend>(ServiceLifeTime.Singleton);
			serviceContainer.Register<ICircularDepend2, CircularDepend2>(ServiceLifeTime.Singleton);
			//Act
			Action act = () => {
				var service = serviceContainer.GetService<ICircularDepend>();
				var service2 = serviceContainer.GetService<ICircularDepend2>();
			};

			//Assert
			act.Should().Throw<NotSupportedException>()
				.WithMessage("You can't depend on something that depends on you dummy");
		}
	}
}