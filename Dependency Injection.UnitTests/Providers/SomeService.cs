﻿using JetBrains.Annotations;

namespace Dependency_Injection.UnitTests.Providers {
	[UsedImplicitly]
	internal class SomeService : ISomeService {
		private readonly IRandomGuidProvider _randomGuidProvider;

		public SomeService(IRandomGuidProvider randomGuidProvider) {
			_randomGuidProvider = randomGuidProvider;
		}

		public string GetGuid() {
			return _randomGuidProvider.GenerateGuid.ToString();
		}
	}
}