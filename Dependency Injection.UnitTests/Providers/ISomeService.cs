﻿namespace Dependency_Injection.UnitTests.Providers {
	public interface ISomeService {
		string GetGuid();
	}
}