﻿using System;

namespace Dependency_Injection.UnitTests.Providers {
	public interface IRandomGuidProvider {
		Guid GenerateGuid { get; }
	}
}