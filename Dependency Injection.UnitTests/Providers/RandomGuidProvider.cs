﻿using System;

namespace Dependency_Injection.UnitTests.Providers {
	public class RandomGuidProvider : IRandomGuidProvider {
		private Guid _guid;

		public Guid GenerateGuid {
			get {
				if (_guid == default)
					_guid = Guid.NewGuid();
				return _guid;
			}
		}
	}
}