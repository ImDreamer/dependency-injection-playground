﻿using System;

namespace Dependency_Injection.UnitTests.Providers {
	public abstract class AbstractRandomGuidProvider : IRandomGuidProvider {
		private Guid _guid;

		public Guid GenerateGuid {
			get {
				if (_guid == default)
					_guid = Guid.NewGuid();
				return _guid;
			}
		}
	}
}